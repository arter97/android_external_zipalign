# 
# Copyright 2008 The Android Open Source Project
#
# Zip alignment tool
#

LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := zipalign
LOCAL_MODULE_TAGS := optional
LOCAL_MODULE_PATH := $(TARGET_OUT_OPTIONAL_EXECUTABLES)
LOCAL_SRC_FILES := ZipAlign.cpp ZipEntry.cpp ZipFile.cpp
LOCAL_C_INCLUDES += $(LOCAL_PATH) external/zlib/src
LOCAL_STATIC_LIBRARIES := libz libutils libcutils liblog

include $(BUILD_EXECUTABLE)
